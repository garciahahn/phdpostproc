function new_table = ClearDataTable(t)
% ClearDataTable Formats the data table obtained from the simulation csv file
%
%    NEW_TABLE = ClearDataTable(REPEATED_TABLE) clears the csv file from repeated
%    datapoints and stores the new table with the repeated points eliminated in
%    NEW_TABLE. In case a repeated data point is found, the new non-repeated
%    value averages the repeated values.

%    Author(s): Julián Nicolás García Hahn
%    E-mail: garciahahn@gmail.com
    new_table = t;
    i = 1;
    while size(new_table, 1) ~= i
        indexes = find(new_table.x == new_table.x(i) &...
                       new_table.y == new_table.y(i));
        if numel(indexes) > 1
            new_table{indexes(1), :} = mean(new_table{indexes, :});
            new_table(indexes(2:end), :) = [];
        end
        i = i+1;
    end

end % ClearDataTable