function PHD_SetupPaths()
% PHD_SetupPaths set up the paths used for the library
  %
  %   PHD_SetupPaths() adds to the MATLAB path all the library's folders that
  %   are necessary to run the scripts

  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com
  

  filePath = mfilename('fullpath');
  [proot, ~, ~] = fileparts(filePath);

  p = {fullfile(proot),...
       fullfile(proot, 'NSK_Cartesian'), ...
      % Add more folders here in the future following the syntaxis:
      % fullfile(proot, 'new_folder'), ...
      };
  addpath(p{:});
end