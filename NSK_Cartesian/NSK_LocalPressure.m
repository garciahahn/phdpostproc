function p = NSK_LocalPressure(datatable)
% NSK_LocalPressure Computes the local pressure terms
%
%    P = NSK_LocalPressure(DATATABLE) coomputes the local terms of the pressure P
%    for the simulation in DATATABLE.
%    NOTE: this is the old thermodynamic pressure.

%    Author(s): Juli�n Nicol�s Garc�a Hahn
%    E-mail: garciahahn@gmail.com
    t = datatable;
    p = (8*t.R)./(3-t.R) .* t.T - 3 * t.R .* t.R;
end % NSK_LocalPressure