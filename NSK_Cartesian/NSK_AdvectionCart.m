function [x, y] = NSK_AdvectionCart(datatable)
  % NSK_AdvectionCart Computes the 2D advection components of a simulation
  %
  %    [X, Y] = NSK_AdvectionCart(DATATABLE) computes the 2D advection momentum
  %    components X and Y for a given set of data DATATABLE of a cartesian system.
  
  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com

    t = datatable;
    x = t.R .* (t.u .* t.ux + t.v .* t.uy);
    y = t.R .* (t.u .* t.vx + t.v .* t.vy);
end % NSK_AdvectionCart