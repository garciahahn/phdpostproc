function q = NSK_ViscousDissipationEnergy(datatable, Re)
% NSK_ViscousDissipationEnergy computes the 2D cartesian viscous dissipation energy
%
%    Q = NSK_ViscousDissipationEnergy(DATATABLE, RE) computes the viscous
%    dissipation energy Q for the simulation in DATATABLE using the 
%    Reynolds number RE.
  
%    Author(s): Julián Nicolás García Hahn, Vitor Heitor Cardoso Cunha
%    E-mail: garciahahn@gmail.com, heitorvitorc@gmail.com
    t = datatable;

    q = - 1/Re * (t.ux.*t.ux + t.uy.*t.uy + t.vx.*t.vx + t.vy.*t.vy ...
                 + t.ux.*t.ux + t.uy.*t.vx + t.vx.*t.uy + t.vy.*t.vy ...
                 - 2/3 * (t.ux + t.vy).^2 );
    
end % NSK_ViscousDissipationEnergy