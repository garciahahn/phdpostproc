function [x, y] = NSK_DivLocalPressureCart(datatable)
  % NSK_DivLocalPressureCart Computes the 2D divergence of the local pressure terms
  %
  %    [X, Y] = NSK_DivLocalPressureCart(DATATABLE) computes the 2D X and Y
  %    momentum components of the divergence of the local pressure (old 
  %    thermodynamical pressure).
  %    NOTE: mind that this is not multiplied by any non-dimensional number.
  
  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com
    t = datatable;
    x = (8*t.R)./(3-t.R) .* t.Tx + (24*t.T)./(3-t.R).^2 .* t.Rx - 6*t.R.*t.Rx;
    y = (8*t.R)./(3-t.R) .* t.Ty + (24*t.T)./(3-t.R).^2 .* t.Ry - 6*t.R.*t.Ry;
end % NSK_DivLocalPressure