function q = NSK_DivHeatConduction(datatable, conductivity)
  % NSK_DivHeatFlux computes the 2D cartesian divergence of the heat conduction
  %
  %    Q = NSK_DivHeatFlux(DATATABLE, CONDUCTIVITY) computes the divergence of
  %    the heat conduction Q for the simulation in DATATABLE using the thermal 
  %    conductivity CONDUCTIVITY.
  
%    Author(s): Julián Nicolás García Hahn, Vitor Heitor Cardoso Cunha
%    E-mail: garciahahn@gmail.com, heitorvitorc@gmail.com
    t = datatable;
    q = conductivity*(t.Txx + t.Tyy);
    
end % NSK_DivHeatFlux