function q = NSK_AdvectionEnergy(datatable, c)
% NSK_AdvectionEnergy computes the 2D cartesian energy advection
%
%    Q = NSK_AdvectionEnergy(DATATABLE, C) computes the advection of energy
%    Q for the simulation in DATATABLE using the adimensional specific heat
  
%    Author(s): Julián Nicolás García Hahn, Vitor Heitor Cardoso Cunha
%    E-mail: garciahahn@gmail.com, heitorvitorc@gmail.com
    t = datatable;
    q = c*t.R.*(t.Tx.*t.u + t.Ty.*t.v);
    
end % NSK_AdvectionEnergy