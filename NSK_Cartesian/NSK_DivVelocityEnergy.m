function q = NSK_DivVelocityEnergy(datatable)
% NSK_DivVelocityEnergy computes the 2D cartesian pressure expansion energy
%
%    Q = NSK_DivVelocityEnergy(DATATABLE) computes the energy flux Q
%    due to pressure expansion for the simulation in DATATABLE.
  
%    Author(s): Julián Nicolás García Hahn, Vitor Heitor Cardoso Cunha
%    E-mail: garciahahn@gmail.com, heitorvitorc@gmail.com
    t = datatable;

    q = - ( (8*t.R.*t.T) ./ (3 - t.R)).*(t.ux + t.vy);
    
end % NSK_DivVelocityEnergy