function [x, y] = NSK_DivViscousDissipationCart(datatable)
  % NSK_DivViscousDissipationCart computes the 2D cartesian divergence of the viscous tensor
  %
  %    [X, Y] = NSK_DivViscousDissipationCart(DATATABLE) computes the
  %    X and Y momentum components of the divergence of the viscous
  %    tensor for a simulation in DATATABLE.
  %    NOTE: mind that this is not multiplied by any non-dimensional number.
  
  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com
    t = datatable;
    x = t.uxx + t.uyy + (t.uxx + t.vxy)/3;
    y = t.vxx + t.vyy + (t.uxy + t.vyy)/3;
end % NSK_DivViscousDissipationCart