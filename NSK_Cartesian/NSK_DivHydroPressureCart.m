function [x, y] = NSK_DivHydroPressureCart(datatable, We)
  % NSK_DivHydroPressureCart Computes the 2D divergence of the hydrostatic pressure terms
  %
  %    [X, Y] = NSK_DivHydroPressureCart(DATATABLE, WE) computes the 2D X and Y
  %    momentum components of the divergence of the hydrostatic pressure
  %    using the WE weber number.
  
  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com
    t = datatable;
    [x, y] = NSK_DivLocalPressureCart(datatable);
    x = x - t.Rx.*t.q - t.R.*t.qx + (t.Rx.*t.Rxx+t.Ry.*t.Rxy)/We;
    y = y - t.Ry.*t.q - t.R.*t.qy + (t.Rx.*t.Rxy+t.Ry.*t.Ryy)/We;
end % NSK_DivLocalPressure