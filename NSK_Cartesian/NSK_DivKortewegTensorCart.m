function [x, y] = NSK_DivKortewegTensorCart(datatable)
  % NSK_DivKortewegTensorCart Computes the 2D divergence of the Korteweg Stress Tensor
  %
  %    [X, Y] = NSK_DivKortewegTensorCart(DATATABLE) computes the 2D
  %    divergence of the Korteweg stress tensor for a given simulation DATATABLE.
  %    NOTE: mind that this is not multiplied by any non-dimensional number.
  
  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com
    t = datatable;
    x = t.R .* t.qx;
    y = t.R .* t.qy;
end % NSK_DivKorCart