function p = NSK_HydroPressure(datatable, We)
  % NSK_HydroPressure computes the 2D cartesian hydrostatic pressure
  %
  %    P = NSK_HydroPressure(DATATABLE, WEBER) computes the hydrostatic
  %    pressure P for the simulation in DATATABLE using the WEBER number.
  %    NOTE: this is the old thermodynamical pressure plus the long range terms.
  
  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com
    t = datatable;
    p = NSK_LocalPressure(t) - t.R .* t.q + (t.Rx.^2 + t.Ry.^2)/We/2;
end % NSK_HydroPressure