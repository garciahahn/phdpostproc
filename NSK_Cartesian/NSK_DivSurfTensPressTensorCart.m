function [x, y] = NSK_DivSurfTensPressTensorCart(datatable)
  % NSK_DivSurfTensPressTensorCart computes the 2D cartesian divergence of the surface tension pressure tensor
  %
  %    [X, Y] = NSK_DivSurfTensPressTensorCart(DATATABLE) computes the
  %    X and Y momentum components of the divergence of the surface tension
  %    pressure tensor for a simulation in DATATABLE.
  %    NOTE: mind that this is not multiplied by any non-dimensional number.
  
  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com
    t = datatable;
    x = -2 * t.Ry .* t.Rxy + t.Rxy .* t.Ry + t.Ryy .* t.Rx;
    y = -2 * t.Rx .* t.Rxy + t.Rxy .* t.Rx + t.Rxx .* t.Ry;
end % NSK_DivSurfTensPressTensorCart