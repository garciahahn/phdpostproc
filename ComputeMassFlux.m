function mass_flux = ComputeMassFlux(datatable, coords)
% ComputeMassFlux computes the mass flux accros a contour
%
%    MASSFLUX = ComputeMassFlux(DATATABLE, COORDS) computes the mass flux
%    MASSFLUX across the curve defined by COORDS. It uses the
%    normalized density gradient to find the normal direction of the contour,
%    defining the normal direction as that one pointing towards the less dense
%    region. 
%    COORDS should be a matrix with composed by the coordinates of the
%    found interface. Typically:
%        COORDS = [xic, yic];

%    Author(s): Juli�n Nicol�s Garc�a Hahn
%    E-mail: garciahahn@gmail.com
    x = datatable.x; y = datatable.y;
    if size(coords, 1) == 2
        xic = coords(1, :);
        yic = coords(2, :);
    elseif size(coords, 2) == 2
        xic = coords(:, 1);
        yic = coords(:, 2);
    end


    % Compute normal field
    R = datatable.R; Rx = datatable.Rx; Ry = datatable.Ry;
    u = datatable.u; v = datatable.v;
    norm_grad_rho = (Rx.^2 + Ry.^2) .^ .5;
    [nx, ny] = ComputeUnitary(-Rx, -Ry);

    grad_x_unit_i = scatteredInterpolant(x, y, nx);
    grad_y_unit_i = scatteredInterpolant(x, y, ny);
    R_i = scatteredInterpolant(x, y, R);

    u_i = scatteredInterpolant(x, y, u);
    v_i = scatteredInterpolant(x, y, v);

    mass_flux = R_i(xic, yic) .* (u_i(xic, yic) .* grad_x_unit_i(xic, yic) ...
               + v_i(xic, yic) .* grad_y_unit_i(xic, yic));
end % ComputeMassFlux