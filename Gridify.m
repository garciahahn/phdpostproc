function field_grid = Gridify(x, y, field, x_grid, y_grid)
% Gridify evaluates uniformly a field with a given resolution
%
%    FIELD_GRID = Gridify(X, Y, FIELD, X_GRID, Y_GRID) evaluates the field FIELD
%    defined at the coordinate points X and Y using a mesh defined by
%    X_GRID and Y_GRID.

%    Author(s): Juli�n Nicol�s Garc�a Hahn
%    E-mail: garciahahn@gmail.com

    field_interpolant = scatteredInterpolant(x, y, field, 'natural');
    field_grid = field_interpolant(x_grid, y_grid);
end