function out = isRepeated(datatable)
%isRepeated Checks for repeated datapoints in a datatable
  %
  %   OUT = isRepeated(DATATABLE) returns true if there are repeated datapoints
  %   in DATATABLE, false otherwise.
  
  %   Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com
  
  t = datatable;
  for i=1:size(t, 1)
    if numel(find(t.x == t.x(i) & t.y == t.y(i))) > 1
      out = true;
      break;
  end
  out = false;
end