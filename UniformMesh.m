function [x_grid, y_grid] = UniformMesh(datatable, x_res, y_res)
% UniformMesh evaluates uniformly a field with a given resolution
%
%    [X_GRID, Y_GRID] = UniformMesh(DATATABLE, X_RES, Y_RES) creates an uniform
%    with resolution X_RES and Y_RES in the x and y direction respectively
%    from DATATABLE using the extremes values of the domain.

%    Author(s): Juli�n Nicol�s Garc�a Hahn
%    E-mail: garciahahn@gmail.com
    x = linspace(min(datatable.x), max(datatable.x), x_res);
    y = linspace(min(datatable.y), max(datatable.y), y_res);
    [x_grid, y_grid] = meshgrid(x,y);
end % UniformMesh