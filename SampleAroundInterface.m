function [sample_x, sample_y] = SampleAroundInterface(datatable, coords, epsilon)
% SampleAroundInterface samples for coordinates in the vicinity of coordinates
%
%    [SAMPLE_X, SAMPLE_Y] = SampleAroundInterface(DATATABLE, COORDS, EPS)
%    samples coordinates in a thickness EPS in both directions along the
%    normal of an interface defined by COORDS. SAMPLE_X and SAMPLE_Y contain
%    the values for all the sample coordinates. Each column represents a sample
%    line.
%    COORDS should be a matrix with composed by the coordinates of the
%    found interface. Typically:
%        COORDS = [xic, yic];
%    DATATABLE needs to have the related simulation data.
%    NOTE: The sampled points could fall outside the simulation domain. It is
%    responsibility of the user to verify and considerate if that is a problem
%    for the analysis.

%    Author(s): Juli�n Nicol�s Garc�a Hahn
%    E-mail: garciahahn@gmail.com
    
    if size(coords, 1) == 2
        xic = coords(1, :);
        yic = coords(2, :);
    elseif size(coords, 2) == 2
        xic = coords(:, 1);
        yic = coords(:, 2);
    end

    % Set a default parameter to 'epsilon' in case none was given
    if ~exist('epsilon', 'var')
        epsilon = 0.1;
    end

    t = datatable;
    SAMPLE_SIZE = 50;

    % Initializing return matrix
    sample_x = zeros(SAMPLE_SIZE*2+1, numel(xic));
    sample_y = zeros(SAMPLE_SIZE*2+1, numel(xic));

    % Compute the normal to the density vector
    [nx, ny] = ComputeUnitary(t.Rx, t.Ry);
    nx_i = scatteredInterpolant(t.x, t.y, nx);
    ny_i = scatteredInterpolant(t.x, t.y, ny);
    % Sample stencil
    s = linspace(0, epsilon, SAMPLE_SIZE);

    % Main sampling loop
    for i = 1:numel(xic)
        % Extracting local values for better readability
        xic_l = xic(i); yic_l = yic(i);
        nx_l = nx_i(xic_l, yic_l);
        ny_l = ny_i(xic_l, yic_l);
        n_l = [nx_l;ny_l];

        r0 = [xic_l; yic_l];
        sampled_line = [r0+n_l * s(end:-1:1), r0, r0-n_l * s];
        sample_x(:, i) = sampled_line(1, :);
        sample_y(:, i) = sampled_line(2, :);
    end % end of main for loop


end % SampleAroundInterface