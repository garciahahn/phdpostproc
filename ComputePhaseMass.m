function [vapormass, liquidmass] = ComputePhaseMass(datatable, interface_rho, resol)
  % ComputePhaseMass Computes the Heat Flux (grad T) at a selected boundary
  %
  %    [VAPOR, LIQUID] = ComputePhaseMass(DATATABLE, INTERFACE_RHO, RESOL)
  %    divides the density field into two domains based on the density value
  %    INTERFACE_RHO and integrates. VAPOR is the value of the integration for
  %    density values lesser than INTERFACE_RHO and LIQUID is the value of the
  %    integration for density values greater than INTERFACE_RHO.
  
  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com
    R = datatable.R;
    x = datatable.x; y = datatable.y;
    R_interpolator = scatteredInterpolant(x, y, R, 'natural');

    x_unif = linspace(min(x), max(x), resol);
    y_unif = linspace(min(x), max(y), resol);
    [x_grid, y_grid] = meshgrid(x_unif, y_unif);

    R_liquid = R_interpolator(x_grid, y_grid);
    R_liquid(R_liquid<interface_rho) = 0;
    R_vapor = R_interpolator(x_grid, y_grid);
    R_vapor(R_vapor>=interface_rho) = 0;

    liquidmass = trapz(y_unif, trapz(x_unif, R_liquid, 2));
    vapormass = trapz(y_unif, trapz(x_unif, R_vapor, 2));

end % ComputePhaseMass