# phdpostproc - A Post Processing Scripts Compilation

This package is intended to be used to operate on specific data obtained from a _in-house_ __MATLAB__ code.

## Installation (__MATLAB__ pathing)

__MATLAB__ uses a `startup` script each time an interpreter is open. This script is usually found in `~/Documents/MATLAB` (Unix) or in `username\Documents\MATLAB` in Windows and its name is `startup.m` defined as a function. This allows us to set up anything that __MATLAB__ should remember to do on each startup.

In our case we are interested in adding our scripts' folder. To do so, it is necessary to add to the __MATLAB__'s startup script the command:

```matlab
addpath(fullfile("C:\<absolute\path\to\projects>\phdpostproc\"));
PHD_SetupPaths;
```

The `PHD_SetupPaths` function will take care of adding the appropriate folders to __MATLAB__'s function path.

## Contribution Guidelines

In order to contribute and make readable and re-usable code, it is better to follow a couple of guidelines.

### Functions

When writing functions is highly advisable for the functions to take as few input parameters as possible and to output self contain vectorized data. That is, try not to use custom classes and types in the outputs, such that they can be easily reusable by other contributors.

Due to the nature of the format of the data employed, it is _acceptable_ to use the _datatable_ argument to pass the simulation data.

Functions must be self contained. That means that they __should not__ contain absolute path references or be dependable on global variables that are defined locally.

### Templates

Templates of scripts that can be used in other folders will start with a capital _T_ in its name. The purpose of these files will be to be copied into other's projects folders and be run there to execute specific tasks, mostly related to the [Nemesis Project](https://gitlab.com/garciahahn/nemesis).

## Git

The package is intended to be worked along people with rudimentary __Git__ experience. It is not necessary to know the very details of the repository management but, in order to contribute, some basics should be covered.

### Example: you want to add your groundbreaking script to the script compilation

First, you'll need the repository. For that, cloning the repository can be done as:

```bash
$ cd /your/projects/folder/
$ git clone git@gitlab.com:garciahahn/phdpostproc.git
```

A new folder within your `</your/projects/folder/>` will be generated. If you execute `git status`, you'll verify that you are in the `master` branch of the project.

In order to contribute now you need to _branch out_. That is defining your own feature branch before pushing into the server.

```bash
$ git branch my-new-feature
$ git checkout my-new-feature
```

__And now you're ready!__

After adding any new files or changes to the current ones, commiting and _writing a meaningful commit message_ you're ready to make your first own contribution. To do so you'll push your new branch into the remote repository. For this particular case, this is done by executing the following command:

```bash
$ git push -u origin my-new-feature
```
This will return a message that is similar to the following:

```bash
remote:
remote: To create a merge request for my-new-feature, visit:
remote:   https://gitlab.com/garciahahn/phdpostproc/-/merge_requests/new?merge_request%5Bsource_branch%5D=my-new-feature
remote:
```

You just need to follow that link and create a __pull request__ basically asking the owner of the project to revise your code and integrate it into the codebase.

Happy coding! 😊