function curv = ComputeWeightedCurvatureField(datatable, We)
% ComputeWeightedCurvatureField Computes curvature field
%
%    CURV = ComputeWeightedCurvatureField(DATATABLE, WE) computes the
%    curvature field based on the order parameter and multiplies it by the norm
%    of the gradient of the density base on the simulation DATATABLE and the 
%    adimensional Weber number WE.

%    Author(s): Julián Nicolás García Hahn, Vitor Heitor Cardoso Cunha
%    E-mails: garciahahn@gmail.com, heitorvitorc@gmail.com

    t = datatable;
    R_norm = ComputeNorm(t.Rx, t.Ry);
    
    curv = -t.q * We + (t.Rx.^2 .* t.Rxx+ 2 * t.Rxy .* t.Rx .* t.Ry ...
             + t.Ry.^2 .* t.Ryy) ./ R_norm.^2;
    
end % ComputeWeightedCurvatureField