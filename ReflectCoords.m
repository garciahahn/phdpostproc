function [x_new, y_new] = ReflectCoords(x, y, p1, p2)
  % ReflectCoords reflects coordinate vectors about a line
  %
  %    [X_NEW, Y_NEW] = ReflectCoords(X, Y, P1, P2) computes the reflection of
  %    the coordinates X and Y about a line that croses the points P1 and P2.
  
  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com
  x1 = p1(1); x2 = p2(1);
  y1 = p1(2); y2 = p2(2);

  if x1 == x2
    y_new = y;
    x_new = 2*x1 - x;
  elseif y1 == y2
    x_new = x;
    y_new = 2*y1 - y;
  else
    a = (y2-y1)/(x2-x1);
    b = (y1*x2-y2*x1)/(x2-x1);

    x_new = (2*(a*y-a*b)+x*(1-a^2))./(a^2+1);
    y_new = (2*(a*x+b)+y*(-1+a^2))./(a^2+1);
  end
end % ReflectCoords