function mass_flux = ComputeMassFluxMoving(datatable, coords1, coords2, dt)
% ComputeMassFluxMoving computes the mass flux across a moving interface
%
%    MASS_FLUX = ComputeMassFluxMoving(DATATABLE, COORDS1, COORDS2, DT)
%    Computes the mass flux across an interface defined by COORDS1 that moves
%    to position COORDS2 in a timestep DT, using the simulation data DATATABLE
%    COORDS1 and COORDS 2 should be a matrix with composed by the coordinates of
%    interfaces. Typically:
%        COORDS1 = [xic1; yic1];
%        COORDS2 = [xic2; yic2];

%    Author(s): Julián Nicolás García Hahn, Vitor Heitor Cardoso Cunha
%    E-mail: garciahahn@gmail.com, heitorvitorc@gmail.com
  if size(coords1, 1) == 2
        xic1 = coords1(1, :);
        yic1 = coords1(2, :);
    elseif size(coords1, 2) == 2
        xic1 = coords1(:, 1);
        yic1 = coords1(:, 2);
    end

    if size(coords2, 1) == 2
        xic2 = coords2(1, :);
        yic2 = coords2(2, :);
    elseif size(coords2, 2) == 2
        xic2 = coords2(:, 1);
        yic2 = coords2(:, 2);
  end
  x = datatable.x; y = datatable.y;
    % Compute interface velocity
  [u_i, v_i] = ComputeInterfaceVelocity(coords1, coords2, dt);
  R_i = scatteredInterpolant(datatable.x, datatable.y, datatable.R);
  % Compute normal field
  Rx = datatable.Rx; Ry = datatable.Ry;
  u = datatable.u; v = datatable.v;
  [grad_x_unit, grad_y_unit] = ComputeUnitary(-Rx, -Ry);

  grad_x_unit_i = scatteredInterpolant(x, y, grad_x_unit);
  grad_y_unit_i = scatteredInterpolant(x, y, grad_y_unit);

  u = scatteredInterpolant(x, y, u);
  v = scatteredInterpolant(x, y, v);
  
  % Compute mass flux  
  mass_flux = R_i(xic1, yic1) .* ...
               ((u(xic1, yic1) - u_i) .* grad_x_unit_i(xic1, yic1) ...
             + (v(xic1, yic1) - v_i) .* grad_y_unit_i(xic1, yic1));
  
end % ComputeMassFluxMoving