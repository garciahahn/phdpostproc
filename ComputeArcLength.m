function arc_length = ComputeArcLength(coords)
% ComputeArcLength computes the arc length for each point of coords
%
%    ARC_LENGTH = ComputeArcLength(COORDS) computes the arc length ARC_LENGTH
%    based on the ordered coordinates COORDS. The arc length will start where
%    the fista coordinate of the COORDS variable points.
%    COORDS should be a matrix with composed by the coordinates of the
%    found interface. Typically:
%        COORDS = [xic, yic];

%    Author(s): Juli�n Nicol�s Garc�a Hahn
%    E-mail: garciahahn@gmail.com
    if size(coords, 1) == 2
        xic = coords(1, :);
        yic = coords(2, :);
    elseif size(coords, 2) == 2
        xic = coords(:, 1);
        yic = coords(:, 2);
    end

    arc_length = [0, cumsum((diff(xic).^2+diff(yic).^2).^.5)];
end % ComputeArcLength