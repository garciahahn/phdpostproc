function normal = ComputeNorm(x, y)
  % ComputeNorm computes the elementwise norm of x and y vector components
  %
  %    NORMAL = ComputeNorm(X, Y) computes elementwise norm NORMAL of a vector
  %    expressed in its X and Y components.
  
  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com
    normal = (x.^2 + y.^2).^ .5;
end % ComputeNorm