function [X, Y] = ComputeInterfaceLine(datatable, interface_rho, resol)
% ComputeInterfaceLine computes the position of the contour of the fluid
%
%    [X, Y] = ComputeInterfaceLine(DATATABLE, RHO, RESOL) finds the location
%    X and Y of the contour of level RHO in the data DATATABLE using a
%    interpolation mesh with RESOL points on each direction.

%    Author(s): Juli�n Nicol�s Garc�a Hahn
%    E-mail: garciahahn@gmail.com

    R = datatable.R;
    x = datatable.x; y = datatable.y;
    R_interpolator = scatteredInterpolant(x, y, R, 'natural');
    x_unif = linspace(min(x), max(x), resol);
    y_unif = linspace(min(x), max(y), resol);
    [x_grid, y_grid] = meshgrid(x_unif, y_unif);
    R_unif = R_interpolator(x_grid, y_grid);
    IC = contourc(x_unif, y_unif, R_unif, [interface_rho, interface_rho]);

    X = IC(1, 2:end);
    Y = IC(2, 2:end);
end % ComputeInterfaceLine