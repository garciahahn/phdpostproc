function [u, v] = ComputeInterfaceVelocity(coords1, coords2, dt)
% ComputeInterfaceVelocity Computes the interface velocity based on two timesteps
%
%    [U, V] = ComputeInterfaceVelocity(COORDS1, COORDS2, DT) Computes the
%    interface velocity of COORDS1 based on the position of COORDS2 and the
%    time step DT.
%    COORDS1 and COORDS 2 should be a matrix with composed by the coordinates of
%    interfaces. Typically:
%        COORDS1 = [xic1; yic1];
%        COORDS2 = [xic2; yic2];

%    Author(s): Vitor Heitor Cardoso Cunha, Julián Nicolás García Hahn
%    E-mails: heitorvitorc@gmail.com, garciahahn@gmail.com
    if size(coords1, 1) == 2
        xic1 = coords1(1, :);
        yic1 = coords1(2, :);
    elseif size(coords1, 2) == 2
        xic1 = coords1(:, 1);
        yic1 = coords1(:, 2);
    end

    if size(coords2, 1) == 2
        xic2 = coords2(1, :);
        yic2 = coords2(2, :);
    elseif size(coords2, 2) == 2
        xic2 = coords2(:, 1);
        yic2 = coords2(:, 2);
    end
    t2 = datatable2;
    t1 = datatable2;  
    %%% Compute interface velocity
    u = zeros(size(xic1));
    v = zeros(size(yic1));
    
    % Ccollecting the closest points to estimate the interface velocity
    for i=1:length(xic1)
        dist=100;
        id = 0;
        for j=1:length(xic2)
            d = sqrt((xic2(i)-xic1(j))^2+(yic2(i)-yic1(j))^2);
            if d < dist
                dist = d;
                id = j;
            end
        end
        u(i) = (xic2(i)-xic1(id))/dt;
        v(i) = (yic2(i)-yic1(id))/dt;      
    end
    
end