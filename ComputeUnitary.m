function [x_u, y_u] = ComputeUnitary(x, y)
  % ComputeUnitary computes unit vector in the direction of x, y
  %
  %    [X_U, Y_U] = ComputeUnitary(X, Y) computes the unit vector with
  %    components X_U and Y_U that point in the same direction as the the vector
  %    with components X and Y.
  
  %    Author(s): Juli�n Nicol�s Garc�a Hahn
  %    E-mail: garciahahn@gmail.com

  normal = (x.^2 + y.^2).^ .5;
  x_u = x./normal;
  y_u = y./normal;
end % ComputeUnitary