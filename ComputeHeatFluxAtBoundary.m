function [normal_heatflux, coords] = ComputeHeatFluxAtBoundary(datatable, direction, boundary, resol)
% ComputeHeatAtBoundary Computes the Heat Flux (grad T) at a selected boundary
%
%    [HEATFLUX, COORDS] = ComputeHeatAtBoundary(DATATABLE, DIR, BOUND, RESOL)
%    computes the heat conduction HEATFLUX across an boundary of the simulated
%    domain using an interpolation mesh with RESOL points on each direction
%    and returns the coordinates COORDS for the evaluation of the heat flux.
%    The boundary is selected from direction DIR (1 for X and 2 for Y) at the
%    BOUND position (0 for lowest and 1 for highest).
%
%    In this way, to compute the heat conduction across the bottom
%    domain boundary (Y=0) the function must be called as:
%
%        ComputeHeatAtBoundary(DATATABLE, 2, 0, RESOL)

%    Author(s): Julián Nicolás García Hahn
%    E-mail: garciahahn@gmail.com

  if direction == 1
    T_flux = -datatable.Tx;
  elseif direction == 2
    T_flux = -datatable.Ty;
  end
  x = datatable.x; y = datatable.y;
  x_unif = linspace(min(x), max(x), resol);
  y_unif = linspace(min(x), max(y), resol);
  [x_grid, y_grid] = meshgrid(x_unif, y_unif);

  flux_interpolator = scatteredInterpolant(x, y, T_flux, 'natural');
  
  if boundary == 0
    f = @min;
  elseif boundary == 1
    f = @max;
  end

  if direction == 1
    normal_heatflux = flux_interpolator(ones(1, length(y_unif))*f(x_unif), y_unif);
    coords = x_unif;
  else if direction == 2
    normal_heatflux = flux_interpolator(x_unif, ones(1, length(y_unif))*f(y_unif));
    coords = y_unif;
  end

end % ComputeHeatAtBoundary