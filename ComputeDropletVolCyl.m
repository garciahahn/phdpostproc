function VOLUME = ComputeDropletVolCart(DATATABLE, INTERFACE_RHO, RESOL)
%ComputeDropletVolCart computes the volume of a cartesian droplet (2D)
%
%    VOLUME = ComputeDropletVolCart(DATATABLE, INTERFACE_RHO, RESOL) computes the
%    volume of a cartesian droplet from simulation DATATABLE using the interface
%    density INTERFACE_RHO and with a mesh resolution of RESOL.

%    Author(s): Juli�n Nicol�s Garc�a Hahn
%    E-mail: garciahahn@gmail.com
    t = DATATABLE;
    x = t.x; y = t.y;
    R = t.R;

    [x_grid, y_grid] = UniformMesh(t, RESOL, RESOL);
    R_drop = Gridify(x, y, R, x_grid, y_grid);
    weight_field = ones(size(R_drop)) .* x_grid * 2 * pi;
    weight_field(R_drop <= INTERFACE_RHO) = 0.0;

    VOLUME = trapz(y_grid(:, 1), trapz(x_grid(1, :), weight_field, 2));
end % ComputeDropletVolCart