function curv = ComputeCurvature(datatable, We, coords)
% ComputeCurvature Computes the curvature at the given coordinates
%
%    CURV = ComputeCurvature(DATATABLE, WE, COORDS) computes the
%    curvature value along the coordinates COORDS 
%    in the data DATATABLE using the adimensional number Weber WE.
%    COORDS should be a matrix with composed by the coordinates of the
%    found interface. Typically:
%        COORDS = [xic, yic];

%    Author(s): Julián Nicolás García Hahn, Vitor Heitor Cardoso Cunha
%    E-mails: garciahahn@gmail.com, heitorvitorc@gmail.com
    if size(coords, 1) == 2
        xic = coords(1, :);
        yic = coords(2, :);
    elseif size(coords, 2) == 2
        xic = coords(:, 1);
        yic = coords(:, 2);
    end
    t = datatable;
    CurvField = ComputeWeightedCurvatureField(t, We);
    R_norm = ComputeNorm(t.Rx, t.Ry);

    Curv_interpolator = scatteredInterpolant(t.x, t.y, CurvField, 'natural');
    R_norm_interpolator = scatteredInterpolant(t.x, t.y, R_norm, 'natural');
    
    curv = Curv_interpolator(xic,yic)./R_norm_interpolator(xic,yic);
    
end % ComputeCurvature