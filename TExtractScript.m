% This script will consecutively run the 'extractData' function from the Nemesis
% project. It is inteded to be used in the simulation file of a given simulation
% where the 'Settings.m' file is located and the output folder has the same name
% as in the Settings.m file.

% CONFIGURATION
first_timestep = 2000;
step = 2000;
last_timestep = 20000;

% The output naming will be "t<timestep>.csv"
for i=first_timestep:step:last_timestep
    extractData(i, "t" + string(i));
end
